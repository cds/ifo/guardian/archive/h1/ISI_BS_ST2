"""
Base definition for a guardian ISI single stage.

Modules should at a minimum include the following import:

from isiguardianlib.ISI_STAGE import *

Parameters are defined in "const" files for the various linked
modules, and can be changed by importing the needed const file and
modifying the parameters therein BEFORE import the full module, e.g.:

from isiguardianlib.isolation.const import ISOLATION_CONSTANTS
ISOLATION_CONSTANTS['CART_BIAS_DOF_LISTS'] = (['RY'], ['RZ'])
from isiguardianlib.ISI_STAGE import *
 
"""

from .decorators import *
from .states import *
from .edges import *

# ISIs have no default initial REQUEST state, as they are expected to
# be operated in a managed mode by the chamber managers

# this sets the NOMINAL state, which is the expected operational state
nominal = 'HIGH_ISOLATED'
